-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 10, 2024 at 06:19 AM
-- Server version: 8.2.0
-- PHP Version: 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `books`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
CREATE TABLE IF NOT EXISTS `authors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  `birth_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `name`, `birth_date`) VALUES
(1, 'Book Title', '2023-06-09'),
(2, 'Book Title', '2023-06-09');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE IF NOT EXISTS `books` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author_id` int DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `publication_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author_id`, `genre`, `publication_date`) VALUES
(1, 'don net', 1, 'test', '2023-06-09'),
(2, 'don net', 1, 'test', '2023-06-09'),
(3, 'don net', 1, 'test', '2023-06-09'),
(4, 'don net', 1, 'test', '2023-06-09'),
(5, 'don net', 1, 'test', '2023-06-09'),
(6, 'don net', 1, 'test', '2023-06-09'),
(7, 'don net', 1, 'test', '2023-06-09');

-- --------------------------------------------------------

--
-- Table structure for table `borrowed_books`
--

DROP TABLE IF EXISTS `borrowed_books`;
CREATE TABLE IF NOT EXISTS `borrowed_books` (
  `id` int NOT NULL AUTO_INCREMENT,
  `book_id` int NOT NULL,
  `user_id` int NOT NULL,
  `borrowed_date` date NOT NULL,
  `return_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `book_id` (`book_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `borrowed_books`
--

INSERT INTO `borrowed_books` (`id`, `book_id`, `user_id`, `borrowed_date`, `return_date`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2024-06-10', '2024-06-24', '2024-06-10 04:53:14', '2024-06-10 04:53:14'),
(2, 1, 2, '2024-06-10', '2024-06-24', '2024-06-10 04:53:28', '2024-06-10 04:53:28'),
(3, 1, 2, '2024-06-10', '2024-06-24', '2024-06-10 05:36:53', '2024-06-10 05:36:53'),
(4, 1, 2, '2024-06-10', '2024-06-24', '2024-06-10 05:38:03', '2024-06-10 05:38:03'),
(5, 1, 2, '2024-06-10', '2024-06-24', '2024-06-10 05:38:04', '2024-06-10 05:38:04'),
(6, 1, 2, '2024-06-10', '2024-06-24', '2024-06-10 05:38:57', '2024-06-10 05:38:57'),
(7, 1, 2, '2024-06-10', '2024-06-24', '2024-06-10 05:38:58', '2024-06-10 05:38:58'),
(8, 1, 2, '2024-06-10', '2024-06-24', '2024-06-10 06:07:17', '2024-06-10 06:07:17'),
(9, 1, 2, '2024-06-10', '2024-06-24', '2024-06-10 06:07:18', '2024-06-10 06:07:18'),
(10, 1, 2, '2024-06-10', '2024-06-24', '2024-06-10 06:07:19', '2024-06-10 06:07:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(1, 'net', 'nettest@example.com', '$2b$10$nU7/qKWNeAhsLSCPi9OxrOUG.bRLRCtBiJgdHF.2NGWIkGXanBRyO');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
