const connection = require('../config/db');

exports.createAuthor = async (req, res) => {
    const { name, birthDate } = req.body;
    const query = "INSERT INTO authors (name, birth_date) VALUES (?, ?)";
    connection.query(query, [name, birthDate], (err, results) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.status(201).send({
            "status": true,
            "message": "Author created successfully",
            "author": {
                "id": 1,
                "name": "Sample Author",
                "birthDate": "2023-06-09"
            }
        });
    }
    );
}

exports.getAuthors = async (req, res) => {
    const query = "SELECT * FROM authors";
    connection.query(query, (err, results) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.status(200).send(
            {
                "status": true,
                "message": "Authors retrieved successfully",
                "authors": (results.length > 0 ? results : [])
            });
    }

    );
}

exports.getAuthorById = async (req, res) => {
    const { id } = req.params;
    const query = "SELECT * FROM authors WHERE id = ?";
    connection.query(query, [id], (err, results) => {
        if (err) {
            return res.status(500).send(err);
        } else if (results.length === 0) {
            return res.status(404).send("Author not found");
        }
        res.status(200).send(results[0]);
    }
    );
}

exports.updateAuthor = async (req, res) => {
    const { id } = req.params;
    const { name, birthDate } = req.body;
    const query = "UPDATE authors SET name = ?, birth_date = ? WHERE id = ?";
    connection.query(query, [name, birthDate, id], (err, results) => {
        if (err) {
            return res.status(500).send(err);
        } else if (results.affectedRows === 0) {
            return res.status(404).send("Author not found");
        }
        res.status(200).send({ id, name, birthDate });
    }
    );
}

exports.deleteAuthor = async (req, res) => {
    const { id } = req.params;
    const query = "DELETE FROM authors WHERE id = ?";
    connection.query(query, [id], (err, results) => {
        if (err) {
            return res.status(500).send(err);
        } else if (results.affectedRows === 0) {
            return res.status(404).send("Author not found");
        }
        res.status(200).send({ id });
    }
    );
} 
