// userController.js

const connection = require("../config/db");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const saltRounds = 10;
const secretKey = process.env.JWT_SECRET || 'your-secret-key';

exports.createUser = (req, res) => {
  const { name, email, password } = req.body;

  bcrypt.hash(password, saltRounds, (err, hashedPassword) => {
    if (err) {
      return res.status(500).send(err);
    }

    const query = "INSERT INTO users (name, email, password) VALUES (?, ?, ?)";
    connection.query(query, [name, email, hashedPassword], (err, results) => {
      if (err) {
        return res.status(500).send(err);
      }

      res.status(201).send({
        id: results.insertId,
        name,
        email,
      });
    });
  });
};

exports.loginUser = (req, res) => {
  const { email, password } = req.body;

  const query = "SELECT * FROM users WHERE email = ?";
  connection.query(query, [email], (err, results) => {
    if (err) {
      return res.status(500).send('Error querying database');
    }

    if (results.length === 0) {
      return res.status(401).send('Invalid email or password');
    }

    const user = results[0];
    bcrypt.compare(password, user.password, (err, isMatch) => {
      if (err) {
        return res.status(500).send('Error comparing passwords');
      }

      if (!isMatch) {
        return res.status(401).send('Invalid email or password');
      }

      const token = jwt.sign({ id: user.id, email: user.email }, secretKey, { expiresIn: '1h' });
      res.send({ token });
    });
  });
};

exports.getAllUsers = (req, res) => {
  const query = "SELECT * FROM users";
  connection.query(query, (err, results) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(results);
    }
  });
};

exports.getSingleUser = (req, res) => {
  const { id } = req.params;
  const query = "SELECT * FROM users WHERE id = ?";
  connection.query(query, [id], (err, results) => {
    if (err) {
      res.status(500).send(err);
    } else if (results.length === 0) {
      res.status(404).send("User not found");
    } else {
      res.status(200).send(results[0]);
    }
  });
};

exports.updateUser = (req, res) => {
  const { id } = req.params;
  const { name, email, password } = req.body;
  bcrypt.hash(password, saltRounds, (err, hashedPassword) => {
    if (err) {
      return res.status(500).send('Error hashing password');
    }
    const query = "UPDATE users SET name = ?, email = ?, password = ? WHERE id = ?";
    connection.query(query, [name, email, hashedPassword, id], (err, results) => {
      if (err) {
        res.status(500).send(err);
      } else if (results.affectedRows === 0) {
        res.status(404).send("User not found");
      } else {
        res.status(200).send({ id, name, email });
      }
    });
  });
};

exports.deleteUser = (req, res) => {
  const { id } = req.params;
  const query = "DELETE FROM users WHERE id = ?";
  connection.query(query, [id], (err, results) => {
    if (err) {
      res.status(500).send(err);
    } else if (results.affectedRows === 0) {
      res.status(404).send("User not found");
    } else {
      res.status(200).send("User deleted");
    }
  });
};
