const connection = require("../config/db");

exports.createBorrowedBook = (req, res) => {
  const { book_id, user_id, borrowed_date, return_date } = req.body;
  const query = "INSERT INTO borrowed_books (book_id, user_id, borrowed_date, return_date) VALUES (?, ?, ?, ?)";
  connection.query(query, [book_id, user_id, borrowed_date, return_date], (err, results) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(201).send({
      "status": true,
      "message": "Borrowed book created successfully",
      "borrowed_book": {
        "id": results.insertId,
        "book_id": book_id,
        "user_id": user_id,
        "borrowed_date": borrowed_date,
        "return_date": return_date
      }
    });
  });
}

exports.updateBorrowedBook = (req, res) => {
  const { id } = req.params;
  const { book_id, user_id, borrowed_date, return_date } = req.body;
  const query = "UPDATE borrowed_books SET book_id = ?, user_id = ?, borrowed_date = ?, return_date = ? WHERE id = ?";
  connection.query(query, [book_id, user_id, borrowed_date, return_date, id], (err, results) => {
    if (err) {
      return res.status(500).send(err);
    } else if (results.affectedRows === 0) {
      return res.status(404).send("Borrowed book record not found");
    }
    res.status(200).send({
      "status": true,
      "message": "Borrowed book updated successfully",
      "borrowed_book": {
        "id": id,
        "book_id": book_id,
        "user_id": user_id,
        "borrowed_date": borrowed_date,
        "return_date": return_date
      }
    });
  });
}

exports.deleteBorrowedBook = (req, res) => {
  const { id } = req.params;
  const query = "DELETE FROM borrowed_books WHERE id = ?";
  connection.query(query, [id], (err, results) => {
    if (err) {
      return res.status(500).send(err);
    } else if (results.affectedRows === 0) {
      return res.status(404).send("Borrowed book record not found");
    }
    res.status(200).send({
      "status": true,
      "message": "Borrowed book deleted successfully"
    });
  });
}

exports.getAllBorrowedBooks = (req, res) => {
  const query = "SELECT * FROM borrowed_books";
  connection.query(query, (err, results) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(200).send(results);
  });
}

exports.getBorrowedBookById = (req, res) => {
  const { id } = req.params;
  const query = "SELECT * FROM borrowed_books WHERE id = ?";
  connection.query(query, [id], (err, results) => {
    if (err) {
      return res.status(500).send(err);
    } else if (results.length === 0) {
      return res.status(404).send("Borrowed book record not found");
    }
    res.status(200).send(results[0]);
  });
} 