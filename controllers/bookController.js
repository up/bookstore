const connection = require('../config/db');

exports.createBook = (req, res) => {
    const { title, author_id, genre, publication_date } = req.body;
    const query = "INSERT INTO books (title, author_id, genre, publication_date) VALUES (?, ?, ?, ?)";
    connection.query(query, [title, author_id, genre, publication_date], (err, results) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.status(201).send({
            "status": true,
            "message": "Book created successfully",
            "book": {
                "id": results.insertId,
                "title": title,
                "author_id": author_id,
                "genre": genre,
                "publication_date": publication_date
            }
        });
    });
};

exports.getBooks = (req, res) => {
    const query = "SELECT * FROM books";
    connection.query(query, (err, results) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.status(200).send({
            "status": true,
            "message": "Books retrieved successfully",
            "books": results
        });
    });
};

exports.getBookById = (req, res) => {
    const { id } = req.params;
    const query = "SELECT * FROM books WHERE id = ?";
    connection.query(query, [id], (err, results) => {
        if (err) {
            return res.status(500).send(err);
        } else if (results.length === 0) {
            return res.status(404).send("Book not found");
        }
        res.status(200).send(results[0]);
    });
};

exports.updateBook = (req, res) => {
    const { id } = req.params;
    const { title, author_id, genre, publication_date } = req.body;
    const query = "UPDATE books SET title = ?, author_id = ?, genre = ?, publication_date = ? WHERE id = ?";
    connection.query(query, [title, author_id, genre, publication_date, id], (err, results) => {
        if (err) {
            return res.status(500).send(err);
        } else if (results.affectedRows === 0) {
            return res.status(404).send("Book not found");
        }
        res.status(200).send({
            "status": true,
            "message": "Book updated successfully",
            "book": {
                "id": id,
                "title": title,
                "author_id": author_id,
                "genre": genre,
                "publication_date": publication_date
            }
        });
    });
};

exports.deleteBook = (req, res) => {
    const { id } = req.params;
    const query = "DELETE FROM books WHERE id = ?";
    connection.query(query, [id], (err, results) => {
        if (err) {
            return res.status(500).send(err);
        } else if (results.affectedRows === 0) {
            return res.status(404).send("Book not found");
        }
        res.status(200).send("Book deleted");
    });
};
