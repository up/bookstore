const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const connection = require('./config/db');
const saltRounds = 10;
const secretKey = process.env.JWT_SECRET || 'your-secret-key';

// Create a user
exports.createUser = (req, res) => {
  const { name, email, password } = req.body;

  bcrypt.hash(password, saltRounds, (err, hashedPassword) => {
    if (err) {
      return res.status(500).send(err);
    }

    const query = "INSERT INTO users (name, email, password) VALUES (?, ?, ?)";
    connection.query(query, [name, email, hashedPassword], (err, results) => {
      if (err) {
        return res.status(500).send(err);
      }

      res.status(201).send({
        id: results.insertId,
        name,
        email,
      });
    });
  });
};

// User login
exports.loginUser = (req, res) => {
  const { email, password } = req.body;

  const query = "SELECT * FROM users WHERE email = ?";
  connection.query(query, [email], (err, results) => {
    if (err) {
      return res.status(500).send('Error querying database');
    }

    if (results.length === 0) {
      return res.status(401).send('Invalid email or password');
    }

    const user = results[0];
    bcrypt.compare(password, user.password, (err, isMatch) => {
      if (err) {
        return res.status(500).send('Error comparing passwords');
      }

      if (!isMatch) {
        return res.status(401).send('Invalid email or password');
      }

      const token = jwt.sign({ id: user.id, email: user.email }, secretKey, { expiresIn: '1h' });
      res.send({ token });
    });
  });
};

// Middleware to authenticate token
exports.authenticateToken = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null) {
    return res.sendStatus(401);
  }

  jwt.verify(token, secretKey, (err, user) => {
    if (err) {
      console.error('Token verification failed:', err);
      return res.sendStatus(403);
    }

    req.user = user;
    next();
  });
};
