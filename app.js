const express = require("express");
const dotenv = require("dotenv");
const userController = require("./controllers/userController");
// book controller 
const bookController = require("./controllers/bookController");
// author controller 
const authController = require("./controllers/authorController");
// borrow book controller
const borrowBookController = require("./controllers/borrowBookController");

dotenv.config();

const app = express();
const port = process.env.PORT || 4000;

// Middleware
app.use(express.json());

// Routes
app.post("/api/users", userController.createUser);
app.post("/api/login", userController.loginUser);
app.get("/api/users", userController.getAllUsers);
app.get("/api/users/:id", userController.getSingleUser);
app.put("/api/users/:id", userController.updateUser);
app.delete("/api/users/:id", userController.deleteUser);

// book 
app.post("/api/books", bookController.createBook);
app.get("/api/books", bookController.getBooks);
app.get("/api/books/:id", bookController.getBookById);
app.put("/api/books/:id", bookController.updateBook);
app.delete("/api/books/:id", bookController.deleteBook);

// author
app.post("/api/authors", authController.createAuthor);
app.get("/api/authors", authController.getAuthors);
app.get("/api/authors/:id", authController.getAuthorById);
app.put("/api/authors/:id", authController.updateAuthor);
app.delete("/api/authors/:id", authController.deleteAuthor);

// Borrowed book routes
app.post("/api/borrowed-books", borrowBookController.createBorrowedBook);
app.get("/api/borrowed-books", borrowBookController.getAllBorrowedBooks);
app.get("/api/borrowed-books/:id", borrowBookController.getBorrowedBookById);
app.put("/api/borrowed-books/:id", borrowBookController.updateBorrowedBook);
app.delete("/api/borrowed-books/:id", borrowBookController.deleteBorrowedBook);



// Start the server
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
