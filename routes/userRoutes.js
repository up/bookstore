const express = require('express');
const router = express.Router();
const userController = require('./controllers/userController');
const authController = require('./controllers/authController');

router.get('/users', authController.authenticateToken, userController.getUsers);
router.get('/users/:id', authController.authenticateToken, userController.getUserById);
router.put('/users/:id', authController.authenticateToken, userController.updateUser);
router.delete('/users/:id', authController.authenticateToken, userController.deleteUser);

module.exports = router;
