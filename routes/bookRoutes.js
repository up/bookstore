const express = require('express');
const router = express.Router();
const bookController = require('../controllers/bookController');
const authenticateToken = require("../middleware/authMiddleware");


router.post('/books', authenticateToken ,bookController.createBook);
router.get('/books', authenticateToken , bookController.getAllBooks);
router.get('/books/:id', authenticateToken , bookController.getBookById);
router.put('/books/:id', bookController.updateBook);
router.delete('/books/:id', bookController.deleteBook);

module.exports = router;
