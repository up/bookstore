const express = require("express");
const borrowBookController = require("../controllers/borrowBookController");
const authenticateToken = require("../middleware/authMiddleware");
const router = express.Router();


router.post("/borrow", authenticateToken, borrowBookController.createBorrowedBook);
router.get("/borrow", authenticateToken, borrowBookController.getRecordBorrowedBook);
router.put("/borrow/:id", authenticateToken, borrowBookController.updateBorrowedBook);
router.delete("/borrow/:id", authenticateToken, borrowBookController.deleteBorrowedRecordById);

module.exports = router;