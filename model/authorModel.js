const db = require('../config/db');

const Author = {
    create: async (name, birthDate) => {
        const [results] = await db.query("INSERT INTO authors (name, birth_date) VALUES (?, ?)", [name, birthDate]);
        return results;
    },

    findAll: async () => {
        const [results] = await db.query("SELECT * FROM authors");
        return results;
    },

    findById: async (id) => {
        const [results] = await db.query("SELECT * FROM authors WHERE id = ?", [id]);
        return results[0];
    },

    update: async (id, name, birthDate) => {
        const [results] = await db.query("UPDATE authors SET name = ?, birth_date = ? WHERE id = ?", [name, birthDate, id]);
        return results;
    },

    delete: async (id) => {
        const [results] = await db.query("DELETE FROM authors WHERE id = ?", [id]);
        return results;
    }
};

module.exports = Author;
