const connection = require("../config/db");

const createUser = (name, email, hashedPassword, createdBy, callback) => {
  const query = "INSERT INTO users (name, email, password, createdBy) VALUES (?, ?, ?, ?)";
  connection.query(query, [name, email, hashedPassword, createdBy], callback);
};

const getUserByEmail = (email, callback) => {
  const query = "SELECT * FROM users WHERE email = ?";
  connection.query(query, [email], callback);
};

const getUserById = (id, callback) => {
  const query = "SELECT * FROM users WHERE id = ?";
  connection.query(query, [id], callback);
};

const getAllUsers = (callback) => {
  const query = "SELECT * FROM users";
  connection.query(query, callback);
};

const updateUser = (id, name, email, hashedPassword, callback) => {
  const query = "UPDATE users SET name = ?, email = ?, password = ? WHERE id = ?";
  connection.query(query, [name, email, hashedPassword, id], callback);
};

const deleteUser = (id, callback) => {
  const query = "DELETE FROM users WHERE id = ?";
  connection.query(query, [id], callback);
};

const searchUsers = (searchCriteria, callback) => {
  const query = "SELECT * FROM users WHERE name LIKE ? OR email LIKE ?";
  const searchValue = `%${searchCriteria}%`;
  connection.query(query, [searchValue, searchValue], callback);
};

module.exports = {
  createUser,
  getUserByEmail,
  getUserById,
  getAllUsers,
  updateUser,
  deleteUser,
  searchUsers,
};
