const connection = require("../config/db");

const BorrowedBook = {
  create: async (borrowedBook) => {
    const { book_id, user_id, borrowed_date, return_date } = borrowedBook;
    const query = "INSERT INTO borrowed_books (book_id, user_id, borrowed_date, return_date) VALUES (?, ?, ?, ?)";
    const [results] = await connection.query(query, [book_id, user_id, borrowed_date, return_date]);
    return results;
  },

  update: async (id, borrowedBook) => {
    const { book_id, user_id, borrowed_date, return_date } = borrowedBook;
    const query = "UPDATE borrowed_books SET book_id = ?, user_id = ?, borrowed_date = ?, return_date = ? WHERE id = ?";
    const [results] = await connection.query(query, [book_id, user_id, borrowed_date, return_date, id]);
    return results;
  },

  delete: async (id) => {
    const query = "DELETE FROM borrowed_books WHERE id = ?";
    const [results] = await connection.query(query, [id]);
    return results;
  },

  getAll: async () => {
    const query = "SELECT * FROM borrowed_books";
    const [results] = await connection.query(query);
    return results;
  },

  getById: async (id) => {
    const query = "SELECT * FROM borrowed_books WHERE id = ?";
    const [results] = await connection.query(query, [id]);
    return results[0];
  }
};
  
module.exports = BorrowedBook;
