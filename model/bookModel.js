const db = require('../config/db');

const Book = {
    create: async (title, authorId, genre, publicationDate) => {
        const [results] = await db.query("INSERT INTO books (title, author_id, genre, publication_date) VALUES (?, ?, ?, ?)", [title, authorId, genre, publicationDate]);
        return results;
    },

    findAll: async () => {
        const [results] = await db.query("SELECT * FROM books");
        return results;
    },

    findById: async (id) => {
        const [results] = await db.query("SELECT * FROM books WHERE id = ?", [id]);
        return results[0];
    },

    update: async (id, title, authorId, genre, publicationDate) => {
        const [results] = await db.query("UPDATE books SET title = ?, author_id = ?, genre = ?, publication_date = ? WHERE id = ?", [title, authorId, genre, publicationDate, id]);
        return results;
    },

    delete: async (id) => {
        const [results] = await db.query("DELETE FROM books WHERE id = ?", [id]);
        return results;
    }
};

module.exports = Book;
